privatecd()
{
  \typeset __privatecd_action
  __privatecd_action="$1"
  shift
  case "$__privatecd_action" in
    (add|help|list|remove)
      __privatecd_${__privatecd_action} "$@" || return $?
      ;;
    (*)
      echo "Unrecognized params: $*"
      privatecd_help
      ;;
  esac
}

__privatecd_hook()
{
  \typeset IFS __privatecd_hook
  \typeset -a __privatecd_hooks

  IFS=$'\n' # split on new lines in files
  __privatecd_hooks=( $( __privatecd_list sort ) )

  IFS=$'\t' # split line on tabs

  for __privatecd_hook in "${__privatecd_hooks[@]}"
  do
    set ${__privatecd_hook}   # set positonal params from line
    if
      [[ "$PWD/" == "$1/"* ]] # path matches
    then
      shift                   # remove path from params
      "$@" || true            # run the command
    fi
  done
}

__privatecd_help()
{
  cat "${privatecd_path}/README.md"
}

__privatecd_add()
{
  \typeset IFS
  IFS=$'\t' # join params with tab
  if \grep "^$1"$'\t' "$HOME/.private-cd-list" >/dev/null 2>&1
  then \sed -i'' -e "s#^$1"$'\t'".*\$#$*#" "$HOME/.private-cd-list" || return $?
  else echo "$*" >> "$HOME/.private-cd-list"
  fi
}

__privatecd_remove()
{
  \sed -i'' -e "\#^$1"$'\t'"# d" "$HOME/.private-cd-list" || return $?
}

__privatecd_list()
{
  if [[ "$1" == sort ]]
  then __privatecd_list_raw | sort -t $'\n'
  else __privatecd_list_raw verbose
  fi
}

__privatecd_list_raw()
{
  if
    [[ -f /etc/private-cd ]]
  then
    [[ "$1" == verbose ]] && echo "=== /etc/private-cd ===" || true
    cat /etc/private-cd
  fi
  if
    [[ -f "$HOME/.private-cd-list" ]]
  then
    [[ "$1" == verbose ]] && echo "=== $HOME/.private-cd-list ===" || true
    cat "$HOME/.private-cd-list"
  fi
}

# setup path for help
export privatecd_path="${BASH_SOURCE:-$_}"
       privatecd_path="$( \command \cd "$(dirname "$privatecd_path")">/dev/null; pwd )"

# setup hook for cd
export -a chpwd_functions                                    # define hooks as an shell array
[[ " ${chpwd_functions[*]} " == *" __privatecd_hook "* ]] || # prevent double addition
chpwd_functions+=(__privatecd_hook)                          # finally add it to the list

# call hook on initialize
__privatecd_hook
