# privatecd

Private configuration of hooks for cd command <https://gitlab.com/mpapis/privatecd>.

Allows to configure a command to be executed when user enters certain directory.

Works with Zsh and Bash (via <https://github.com/mpapis/bash_zsh_support>)

## Installation

1. (On Bash) Install https://github.com/mpapis/bash_zsh_support/tree/master/chpwd

2. Run:

        git clone https://gitlab.com/mpapis/privatecd.git
        cd privatecd
        echo "source '$PWD/privatecd.sh'" >> ~/.bashrc

## Usage

Waning: Quotation is needed to properly handle spaces and other characters.

    privatecd add "/path" "command" ["param1" ["param2" ...]]
    privatecd remove "/path"
    privatecd list [sort]
    privatecd help
